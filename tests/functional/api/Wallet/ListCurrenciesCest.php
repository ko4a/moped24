<?php

namespace App\Tests\functional\api\Wallet;

use App\Infrastructure\UI\Http\API\v1\Wallet\WalletController;
use App\Tests\FunctionalTester;
use Mcustiel\Phiremock\Client\Phiremock;
use Mcustiel\Phiremock\Client\Utils\A;
use Mcustiel\Phiremock\Client\Utils\Is;
use Mcustiel\Phiremock\Client\Utils\Respond;

/**
 * @see WalletController::listAvailableCurrencies()
 */
final class ListCurrenciesCest
{
    private const string LIST_CURRENCIES_URL = '/api/v1/wallet/currencies/';
    private const string NOWPAYMENT_LIST_RESPONSE_FILE = 'nowpayment-list-currencies.json';

    public function testListCurrencies(FunctionalTester $I): void
    {
        $nowPaymentListResponse = $I->readPhiremockResponse(self::NOWPAYMENT_LIST_RESPONSE_FILE);

        $I->expectARequestToRemoteServiceWithAResponse(Phiremock::on(
            A::getRequest()
                ->andUrl(Is::equalTo('/v1/full-currencies'))
        )
            ->then(Respond::withStatusCode(200)->andBody($nowPaymentListResponse))
        );

        $I->amAuthenticatedAsUser();

        $I->sendGET(self::LIST_CURRENCIES_URL);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }
}