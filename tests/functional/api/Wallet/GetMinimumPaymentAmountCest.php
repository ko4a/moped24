<?php

declare(strict_types=1);

namespace App\Tests\functional\api\Wallet;

use App\Infrastructure\UI\Http\API\v1\Wallet\WalletController;
use App\Tests\FunctionalTester;
use Codeception\Example;
use Mcustiel\Phiremock\Client\Phiremock;
use Mcustiel\Phiremock\Client\Utils\A;
use Mcustiel\Phiremock\Client\Utils\Is;
use Mcustiel\Phiremock\Client\Utils\Respond;

/**
 * @see WalletController::getMinimumPaymentAmountForCurrency()
 */
final class GetMinimumPaymentAmountCest
{    private const string MINIMUM_PAYMENT_AMOUNT_URL = '/api/v1/wallet/payment/minimum';

    /**
     * @dataProvider minimumPaymentAmountProvider
     */
    public function testGetMinimumPaymentAmount(FunctionalTester $I, Example $example): void
    {
        $filename = $example['responseFile'];
        $currency = $example['currency'];
        $expectedResponse = $example['expectedResponse'];

        $paymentAmountResponse = $I->readPhiremockResponse($filename);

        $I->expectARequestToRemoteServiceWithAResponse(Phiremock::on(
            A::getRequest()
                ->andUrl(Is::equalTo("/v1/min-amount?currency_from={$currency}&is_fee_paid_by_user=false"))
        )
            ->then(Respond::withStatusCode(200)->andBody($paymentAmountResponse))
        );

        $I->amAuthenticatedAsUser();

        $I->sendGET(sprintf('%s?currencyCode=%s', self::MINIMUM_PAYMENT_AMOUNT_URL, $currency));

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson($expectedResponse);
    }

    protected function minimumPaymentAmountProvider(): array
    {
        return [
            [
                'responseFile' => 'nowpayment-minimum-payment-usdttrc20.json',
                'currency' => 'usdttrc20',
                'expectedResponse' => [
                    'minimumAmount' => 6.288664499999999,
                    'currencyCode' => 'usdttrc20',
                ]
            ],

        ];
    }

}