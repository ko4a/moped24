<?php

declare(strict_types=1);

namespace App\Tests\functional\api\Wallet;

use App\Domain\Wallet\Entity\Wallet;
use App\Infrastructure\UI\Http\API\v1\Wallet\WalletController;
use App\Tests\FunctionalTester;
use Mcustiel\Phiremock\Client\Phiremock;
use Mcustiel\Phiremock\Client\Utils\A;
use Mcustiel\Phiremock\Client\Utils\Is;
use Mcustiel\Phiremock\Client\Utils\Respond;

/**
 * @see WalletController::deposit()
 */
final class DepositWithPaymentCest
{
    private const string PHIREMOCK_DEPOSIT_FILE_RESPONSE = 'nowpayment-deposit-with-payment.json';
    private const string PHIREMOCK_AUTHENTICATE_FILE_RESPONSE = 'nowpayment-authentication.json';
    private const string PHIREMOCK_MINIMUM_PAYMENT_FILE_RESPONSE = 'nowpayment-minimum-payment-usdttrc20.json';
    private const string DEPOSIT_URL = '/api/v1/wallet/payment/deposit/';

    public function testDepositSuccess(FunctionalTester $I): void
    {
        $currency = 'usdttrc20';

        $depositWithPaymentResponse = $I->readPhiremockResponse(self::PHIREMOCK_DEPOSIT_FILE_RESPONSE);

        $I->expectARequestToRemoteServiceWithAResponse(Phiremock::on(
            A::postRequest()
                ->andUrl(Is::equalTo("/v1/sub-partner/payment"))
        )
            ->then(Respond::withStatusCode(200)->andBody($depositWithPaymentResponse))
        );

        $authenticateResponse = $I->readPhiremockResponse(self::PHIREMOCK_AUTHENTICATE_FILE_RESPONSE);

        $I->expectARequestToRemoteServiceWithAResponse(Phiremock::on(
            A::postRequest()
                ->andUrl(Is::equalTo("/v1/auth"))
        )
            ->then(Respond::withStatusCode(200)->andBody($authenticateResponse))
        );

        $minimumPaymentResponse = $I->readPhiremockResponse(self::PHIREMOCK_MINIMUM_PAYMENT_FILE_RESPONSE);

        $I->expectARequestToRemoteServiceWithAResponse(Phiremock::on(
            A::getRequest()
                ->andUrl(Is::equalTo("/v1/min-amount?currency_from={$currency}&is_fee_paid_by_user=false"))
        )
            ->then(Respond::withStatusCode(200)->andBody($minimumPaymentResponse))
        );

        $user = $I->amAuthenticatedAsUser();

        $I->haveInRepository(Wallet::class, [
            'externalId' => 'test',
            'userId' => $user->id,
            'balance' => '100',
            'bonusBalance' => '100',
        ]);

        $I->sendPost(self::DEPOSIT_URL, [
            'currencyCode' => $currency,
        ]);

        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();

        $I->seeResponseContainsJson([
            'currency' => $currency,
        ]);

        $I->seeResponseMatchesJsonType([
            'uuid' => 'string',
            'currency' => 'string',
            'payAddress' => 'string',
            'validUntil' => 'string:date'
        ]);

    }
}