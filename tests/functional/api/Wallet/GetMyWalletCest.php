<?php

declare(strict_types=1);

namespace App\Tests\functional\api\Wallet;

use App\Domain\Wallet\Entity\Wallet;
use App\Infrastructure\UI\Http\API\v1\Wallet\WalletController;
use App\Tests\FunctionalTester;

/**
 * @see WalletController::getMyWallet()
 */
final class GetMyWalletCest
{
    private const string GET_WALLET_URL = '/api/v1/wallet/';

    public function testGetWallet(FunctionalTester $I): void
    {
        $user = $I->amAuthenticatedAsUser();

        $walletId = $I->haveInRepository(Wallet::class, [
            'externalId' => 'test',
            'userId' => $user->id,
            'balance' => '100',
            'bonusBalance' => '100',
        ]);

        $I->sendGET(self::GET_WALLET_URL);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'id' => $walletId,
            'userId' => $user->id,
            'balance' => '100',
            'bonusBalance' => '100'
        ]);
    }
}