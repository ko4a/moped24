<?php

declare(strict_types=1);

namespace App\Tests\functional\api\Security;

use App\Domain\User\Entity\User;
use App\Infrastructure\UI\Http\API\v1\User\SecurityController;
use App\Tests\FunctionalTester;

/**
 * @see SecurityController::register()
 */
final class UserCreateCest
{
    private const string REGISTER_URL = '/api/v1/security/register/';

    public function testCreateUserSuccess(FunctionalTester $I): void
    {
        $I->wantToTest('User creates successfully');

        $I->sendPost(self::REGISTER_URL, [
            'username' => 'test',
            'password' => 'test',
        ]);

        $I->seeResponseCodeIs(200);

        $I->seeInRepository(User::class, [
            'username' => 'test',
        ]);
    }

    public function testUserAlreadyExists(FunctionalTester $I): void
    {
        $I->wantToTest('Cant create already exists user');

        $I->haveInRepository(User::class,[
            'username' => 'test',
            'password' => 'test',
        ]);

        $I->sendPost(self::REGISTER_URL, [
            'username' => 'test',
            'password' => 'test',
        ]);

        $I->seeResponseCodeIs(400);
    }
}