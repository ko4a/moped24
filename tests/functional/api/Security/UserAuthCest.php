<?php

declare(strict_types=1);

namespace App\Tests\functional\api\Security;

use App\Domain\User\Entity\User;
use App\Infrastructure\Service\Security\PasswordProcessor;
use App\Infrastructure\UI\Http\API\v1\User\SecurityController;
use App\Tests\FunctionalTester;
use Symfony\Component\HttpFoundation\Response;

/**
 * @see SecurityController::authenticate()
 */
final class UserAuthCest
{
    private const string AUTH_URL = '/api/v1/security/authenticate/';

    public function testUserAuthSuccessFully(FunctionalTester $I): void
    {
        $I->wantToTest('User can be authenticated');

        /** @var PasswordProcessor $passwordProcessor */
        $passwordProcessor = $I->grabService(PasswordProcessor::class);

        $I->haveInRepository(
            User::class, [
                'username' => 'test',
                'password' => $passwordProcessor->hash('test'),
            ]
        );

        $I->sendPost(self::AUTH_URL, [
            'username' => 'test',
            'password' => 'test',
        ]);

        $I->seeResponseCodeIsSuccessful();
        $I->seeResponseIsJson();

        $response = $I->grabResponse();
    }

    public function testUserAuthFail(FunctionalTester $I): void
    {
        $I->wantToTest('User can not be authenticated with invalid password');

        /** @var PasswordProcessor $passwordProcessor */
        $passwordProcessor = $I->grabService(PasswordProcessor::class);

        $I->haveInRepository(
            User::class, [
                'username' => 'test',
                'password' => $passwordProcessor->hash('test'),
            ]
        );

        $I->sendPost(self::AUTH_URL, [
            'username' => 'test',
            'password' => 'aaa',
        ]);

        $I->seeResponseCodeIs(Response::HTTP_BAD_REQUEST);
    }
}