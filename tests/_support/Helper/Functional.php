<?php

namespace App\Tests\Helper;

use App\Domain\User\Entity\User;

use App\Domain\User\Role;
use App\Infrastructure\Service\Security\PasswordProcessor;
use Codeception\Exception\ModuleException;
use Codeception\Module;
use Codeception\Util\HttpCode;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class Functional extends Module
{
    public function readPhiremockResponse(string $name): string
    {
        return file_get_contents(codecept_data_dir(sprintf('phiremock/responses/%s', $name)));
    }

    /**
     * @throws ModuleException
     */
    public function amAuthenticatedAsUser(): User
    {
        /** @var Module\Doctrine2 $doctrine */
        $doctrine = $this->getModule('Doctrine2');

        /** @var Module\Symfony $symfony */
        $symfony = $this->getModule('Symfony');
        $passwordProcessor = $symfony->grabService(PasswordProcessor::class);

        $userId = $doctrine->haveInRepository(
            User::class, [
                'username' => 'test',
                'password' => $passwordProcessor->hash('test'),
            ]
        );

        $user = $doctrine->grabEntityFromRepository(User::class, ['id' => $userId]);

        /** @var Module\Symfony $symfony */
        $symfony = $this->getModule('Symfony');
        /** @var JWTTokenManagerInterface $tokenGenerator */
        $tokenGenerator = $symfony->grabService(JWTTokenManagerInterface::class);
        $token = $tokenGenerator->create($user);

        /** @var Module\REST $rest */
        $rest = $this->getModule('REST');

        $rest->amBearerAuthenticated($token);

        return $user;
    }
}
