<?php

declare(strict_types=1);

namespace App\Application;

use App\Domain\Common\EventDispatcher;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Service\Attribute\Required;

readonly class AbstractUseCase
{
    protected EntityManagerInterface $entityManager;
    protected EventDispatcher $eventDispatcher;

    #[Required]
    public function setEntityManager(EntityManagerInterface $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    #[Required]
    public function setEventDispatcher(EventDispatcher $eventDispatcher): void
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    protected function beginTransaction(): void
    {
        $this->eventDispatcher->clear();

        $this->entityManager->flush();
        $this->entityManager->beginTransaction();
    }

    protected function commitTransaction(): void
    {
        $this->entityManager->flush();
        $this->entityManager->commit();

        $this->eventDispatcher->flush();
    }

    protected function rollbackTransaction(): void
    {
        if ($this->entityManager->getConnection()->getTransactionNestingLevel() > 0) {
            $this->entityManager->rollback();
        }

        $this->eventDispatcher->clear();
    }

    /**
     * @throws \Throwable
     */
    protected function transaction(callable $callable): mixed
    {
        $this->beginTransaction();

        try {
            $result = $callable();
            $this->commitTransaction();
        } catch (\Throwable $exception) {
            $this->rollbackTransaction();

            throw $exception;
        }

        return $result;
    }
}
