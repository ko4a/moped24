<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Auth\DTO;

use JMS\Serializer\Annotation as JMS;

final class AuthResponse
{
    public const int CODE = 200;

    public function __construct(
        #[JMS\Type('integer')]
        public readonly int $id,
        #[JMS\Type('string')]
        public readonly string $username,
        #[JMS\Type('string')]
        public readonly string $token
    ) {
    }
}
