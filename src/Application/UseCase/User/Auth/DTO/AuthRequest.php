<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Auth\DTO;

use Symfony\Component\Validator\Constraints as Assert;

final readonly class AuthRequest
{
    public function __construct(
        #[Assert\NotBlank(message: 'phone should not be blank')]
        #[Assert\NotNull]
        #[Assert\Type(type: 'string')]
        public string $username,
        #[Assert\NotBlank(message: 'code should not be blank')]
        #[Assert\Type('string')]
        #[Assert\NotNull]
        public string $password
    ) {
    }
}
