<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Auth;

use App\Application\AbstractUseCase;
use App\Application\UseCase\User\Auth\DTO\AuthRequest;
use App\Application\UseCase\User\Auth\DTO\AuthResponse;
use App\Domain\User\Authentication\DTO\UserAuthTask;
use App\Domain\User\Authentication\DTO\UserWithToken;
use App\Domain\User\Authentication\UserAuthenticator;

final readonly class AuthUseCase extends AbstractUseCase
{
    public function __construct(private UserAuthenticator $userAuthenticator)
    {
    }

    public function handle(AuthRequest $request): AuthResponse
    {
        $task = $this->buildTask($request);

        /** @var UserWithToken $userWithToken */
        $userWithToken = $this->transaction(function () use ($task) {
            return $this->userAuthenticator->auth($task);
        });

        return new AuthResponse(
            $userWithToken->user->id,
            $userWithToken->user->getUsername(),
            $userWithToken->token,
        );
    }

    private function buildTask(AuthRequest $request): UserAuthTask
    {
        return new UserAuthTask(
            $request->username,
            $request->password,
        );
    }
}
