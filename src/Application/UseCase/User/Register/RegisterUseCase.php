<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Register;

use App\Application\AbstractUseCase;
use App\Application\UseCase\User\Register\DTO\RegisterRequest;
use App\Application\UseCase\User\Register\DTO\RegisterResponse;
use App\Domain\User\Entity\User;
use App\Domain\User\Register\DTO\RegisterTask;
use App\Domain\User\Register\UserRegisterer;

final readonly class RegisterUseCase extends AbstractUseCase
{
    public function __construct(private UserRegisterer $registerer)
    {
    }

    public function handle(RegisterRequest $registerRequest): RegisterResponse
    {
        $task = $this->buildRegisterTask($registerRequest);

        /** @var User $user */
        $user = $this->transaction(function () use ($task) {
            return $this->registerer->register($task);
        });

        return new RegisterResponse(
            $user->id,
        );
    }

    private function buildRegisterTask(RegisterRequest $registerRequest): RegisterTask
    {
        return new RegisterTask(
            $registerRequest->username,
            $registerRequest->password,
        );
    }
}
