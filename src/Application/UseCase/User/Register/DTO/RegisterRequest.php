<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Register\DTO;

use Symfony\Component\Validator\Constraints as Assert;

final readonly class RegisterRequest
{
    public function __construct(
        #[Assert\NotBlank(message: 'email should not be blank')]
        #[Assert\Type('string')]
        public string $username,
        #[Assert\NotBlank(message: 'password should not be blank')]
        #[Assert\Type('string')]
        public string $password,
    ) {
    }
}
