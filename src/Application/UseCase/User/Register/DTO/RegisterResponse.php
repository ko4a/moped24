<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Register\DTO;

use JMS\Serializer\Annotation as JMS;

final class RegisterResponse
{
    public const int CODE = 200;

    public function __construct(
        #[JMS\Type('integer')]
        public readonly int $id,
    ) {
    }
}
