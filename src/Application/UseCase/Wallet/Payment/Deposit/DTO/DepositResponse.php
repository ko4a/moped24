<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Payment\Deposit\DTO;

final readonly class DepositResponse
{
    public function __construct(
        public string $uuid,
        public string $currency,
        public string $payAddress,
        public string $validUntil,
    ) {
    }
}
