<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Payment\Deposit\DTO;

final readonly class DepositRequest
{
    public function __construct(public string $currencyCode)
    {
    }
}
