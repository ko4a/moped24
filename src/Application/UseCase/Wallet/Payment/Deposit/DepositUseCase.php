<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Payment\Deposit;

use App\Application\AbstractUseCase;
use App\Application\UseCase\Wallet\Payment\Deposit\DTO\DepositRequest;
use App\Application\UseCase\Wallet\Payment\Deposit\DTO\DepositResponse;
use App\Domain\Wallet\PaymentManagement\DepositWithPaymentCreator;

final readonly class DepositUseCase extends AbstractUseCase
{
    public function __construct(private DepositWithPaymentCreator $paymentCreator)
    {
    }

    public function handle(DepositRequest $request, int $userId): DepositResponse
    {
        $deposit = $this->paymentCreator->create($userId, $request->currencyCode);

        return new DepositResponse(
            $deposit->uuid->toString(),
            $deposit->getCurrency(),
            $deposit->getPayAddress(),
            $deposit->getValidUntil(),
        );
    }
}
