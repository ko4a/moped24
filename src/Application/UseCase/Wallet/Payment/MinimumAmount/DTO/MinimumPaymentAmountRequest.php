<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Payment\MinimumAmount\DTO;

use Symfony\Component\Validator\Constraints as Assert;

final readonly class MinimumPaymentAmountRequest
{
    public function __construct(
        #[Assert\NotBlank(message: 'currency is required')]
        #[Assert\NotNull(message: 'currency is required')]
        #[Assert\Type('string')]
        public string $currencyCode,
    ) {
    }
}
