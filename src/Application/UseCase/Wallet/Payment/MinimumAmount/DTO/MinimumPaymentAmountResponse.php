<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Payment\MinimumAmount\DTO;

use JMS\Serializer\Annotation as JMS;

final readonly class MinimumPaymentAmountResponse
{
    public const int CODE = 200;

    public function __construct(
        #[JMS\Type('float')]
        public float $minimumAmount,
        #[JMS\Type('string')]
        public string $currencyCode
    ) {
    }
}
