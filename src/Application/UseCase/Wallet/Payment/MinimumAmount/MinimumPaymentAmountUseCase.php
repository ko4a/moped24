<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Payment\MinimumAmount;

use App\Application\AbstractUseCase;
use App\Application\UseCase\Wallet\Payment\MinimumAmount\DTO\MinimumPaymentAmountRequest;
use App\Application\UseCase\Wallet\Payment\MinimumAmount\DTO\MinimumPaymentAmountResponse;
use App\Domain\Wallet\PaymentManagement\ThirdPartyPaymentManager;
use App\Infrastructure\HttpClient\Exception\HttpClientException;

final readonly class MinimumPaymentAmountUseCase extends AbstractUseCase
{
    public function __construct(private ThirdPartyPaymentManager $paymentManager)
    {
    }

    /**
     * @throws HttpClientException
     */
    public function handle(MinimumPaymentAmountRequest $request): MinimumPaymentAmountResponse
    {
        $amount = $this->paymentManager->minimumPaymentAmount($request->currencyCode);

        return new MinimumPaymentAmountResponse($amount, $request->currencyCode);
    }
}
