<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Currencies\List;

use App\Application\AbstractUseCase;
use App\Application\UseCase\Wallet\Currencies\List\DTO\Currency;
use App\Application\UseCase\Wallet\Currencies\List\DTO\WalletCurrenciesListResponse;
use App\Domain\Wallet\DTO\Currency as DomainCurrency;
use App\Domain\Wallet\WalletCurrenciesListProvider;
use App\Infrastructure\HttpClient\Exception\HttpClientException;

final readonly class WalletCurrenciesListUseCase extends AbstractUseCase
{
    private const array SUPPORTED_CURRENCIES = [
        'BTC',
        'ETH',
        'USDTTRC20',
        'USDTTON',
        'USDTERC20',
        'USDC',
        'USDTSOL',
    ];

    public function __construct(private WalletCurrenciesListProvider $walletCurrenciesListProvider)
    {
    }

    /**
     * @throws HttpClientException
     */
    public function handle(): WalletCurrenciesListResponse
    {
        $currencies = $this->walletCurrenciesListProvider->list();

        $currencies = array_filter($currencies, function (DomainCurrency $currency): bool {
            return in_array($currency->code, self::SUPPORTED_CURRENCIES, true);
        });

        return new WalletCurrenciesListResponse(array_map(
            static fn (DomainCurrency $currency): Currency => new Currency(
                $currency->externalId,
                $currency->code,
                $currency->priority,
                $currency->logoUrl,
            ),
            $currencies,
        )
        );
    }
}
