<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Currencies\List\DTO;

use JMS\Serializer\Annotation as JMS;

final readonly class Currency
{
    public function __construct(
        #[JMS\Type('string')]
        public string $externalId,
        #[JMS\Type('string')]
        public string $code,
        #[JMS\Type('integer')]
        public int $priority,
        #[JMS\Type('string')]
        public string $logoUrl,
    ) {
    }
}
