<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Currencies\List\DTO;

use JMS\Serializer\Annotation as JMS;

final readonly class WalletCurrenciesListResponse
{
    public const int CODE = 200;

    /**
     * @param list<Currency> $currencies
     */
    public function __construct(
        #[JMS\Type('array<App\Application\UseCase\Wallet\Currencies\List\DTO\Currency>')]
        public array $currencies
    ) {
    }
}
