<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Create;

use App\Application\AbstractUseCase;
use App\Application\UseCase\Wallet\Create\DTO\WalletCreateResponse;
use App\Domain\Wallet\Exception\WalletAlreadyExistsException;
use App\Domain\Wallet\Exception\WalletAlreadyExistsException as DomainWalletAlreadyExistsException;
use App\Domain\Wallet\WalletCreator;
use App\Infrastructure\HttpClient\Exception\HttpClientException;
use App\Infrastructure\HttpClient\Exception\UnexpectedBehaviourException;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;

final readonly class WalletCreateUseCase extends AbstractUseCase
{
    public function __construct(
        private WalletCreator $walletCreator,
    ) {
    }

    /**
     * @throws WalletAlreadyExistsException
     * @throws DomainWalletAlreadyExistsException
     * @throws HttpClientException
     * @throws UnexpectedBehaviourException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function handle(int $userId): WalletCreateResponse
    {
        $wallet = $this->walletCreator->create($userId);

        return new WalletCreateResponse(
            $wallet->id,
            $wallet->getUserId(),
        );
    }
}
