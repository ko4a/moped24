<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\Create\DTO;

final readonly class WalletCreateResponse
{
    public function __construct(public int $id, public int $userId)
    {
    }
}
