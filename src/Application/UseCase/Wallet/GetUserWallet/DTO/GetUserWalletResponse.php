<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\GetUserWallet\DTO;

use JMS\Serializer\Annotation as JMS;

final readonly class GetUserWalletResponse
{
    public const int CODE = 200;

    public function __construct(
        #[JMS\Type('integer')]
        public int $id,
        #[JMS\Type('integer')]
        public int $userId,
        #[JMS\Type('string')]
        public string $balance,
        #[JMS\Type('string')]
        public string $bonusBalance,
    ) {
    }
}
