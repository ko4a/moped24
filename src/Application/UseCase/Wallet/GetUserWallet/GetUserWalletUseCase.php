<?php

declare(strict_types=1);

namespace App\Application\UseCase\Wallet\GetUserWallet;

use App\Application\AbstractUseCase;
use App\Application\UseCase\Wallet\GetUserWallet\DTO\GetUserWalletResponse;
use App\Domain\Wallet\Entity\Wallet;
use App\Domain\Wallet\Exception\WalletAlreadyExistsException as DomainWalletAlreadyExistsException;
use App\Domain\Wallet\Repository\WalletRepository;
use App\Domain\Wallet\WalletCreator;
use App\Infrastructure\HttpClient\Clients\NowPayment\Exception\WalletAlreadyExistException;

final readonly class GetUserWalletUseCase extends AbstractUseCase
{
    public function __construct(
        private WalletRepository $walletRepository,
        private WalletCreator $walletCreator
    ) {
    }

    public function handle(int $userId): GetUserWalletResponse
    {
        $wallet = $this->walletRepository->findByUserId($userId);

        if (null === $wallet) {
            $wallet = $this->createWallet($userId);
        }

        return new GetUserWalletResponse(
            $wallet->id,
            $wallet->getUserId(),
            $wallet->getBalance(),
            $wallet->getBonusBalance(),
        );
    }

    public function createWallet(int $userId): Wallet
    {
        try {
            return $this->walletCreator->create($userId);
        } catch (WalletAlreadyExistException $e) {
            throw new DomainWalletAlreadyExistsException($userId, $e->getMessage());
        }
    }
}
