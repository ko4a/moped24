<?php

declare(strict_types=1);

namespace App\Application\Response;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\Response;

final class ValidationFailedResponse
{
    public const CODE = Response::HTTP_UNPROCESSABLE_ENTITY;

    private const NAME = 'exception.validation.failed';

    public function __construct(
        #[JMS\Type('string')]
        public readonly string $errorMessage,
        #[JMS\Type('string')]
        public readonly string $name = self::NAME
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }
}
