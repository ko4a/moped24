<?php

declare(strict_types=1);

namespace App\Application\Response;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\Response;

final class AccessDeniedResponse
{
    public const CODE = Response::HTTP_FORBIDDEN;
    public const NAME = 'exception.access.denied';

    public function __construct(
        #[JMS\Type('string')]
        public readonly string $message,
        #[JMS\Type('string')]
        public readonly string $name = self::NAME,
    ) {
    }
}
