<?php

declare(strict_types=1);

namespace App\Application\Response;

use App\Domain\Common\DomainException;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\Response;

class DomainErrorResponse
{
    public const CODE = Response::HTTP_BAD_REQUEST;

    #[JMS\Type('string')]
    public readonly string $name;
    /**
     * @var array<string, mixed>
     */
    #[JMS\Type('array')]
    public readonly array $context;

    public function __construct(DomainException $exception)
    {
        $this->name = $exception->getName();
        $this->context = $exception->getContext();
    }
}
