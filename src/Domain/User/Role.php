<?php

declare(strict_types=1);

namespace App\Domain\User;

enum Role: string
{
    case ROLE_CLIENT = 'ROLE_CLIENT';
    case ROLE_RESTAURANT_MANAGER = 'ROLE_RESTAURANT_MANAGER';
    case ROLE_MANAGER = 'ROLE_MANAGER';
    case ROLE_DEVELOPER = 'ROLE_DEVELOPER';
    case ROLE_OPERATOR = 'ROLE_OPERATOR';
}
