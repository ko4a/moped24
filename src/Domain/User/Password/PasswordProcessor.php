<?php

declare(strict_types=1);

namespace App\Domain\User\Password;

use App\Domain\User\Register\Exception\InvalidPasswordException;

interface PasswordProcessor
{
    /**
     * @throws InvalidPasswordException
     */
    public function hash(string $plainPassword): string;

    public function verify(string $plainPassword, string $hashedPassword): bool;
}
