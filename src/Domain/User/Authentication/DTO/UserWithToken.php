<?php

declare(strict_types=1);

namespace App\Domain\User\Authentication\DTO;

use App\Domain\User\Entity\User;

final class UserWithToken
{
    public function __construct(
        public readonly User $user,
        public readonly string $token
    ) {
    }
}
