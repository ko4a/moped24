<?php

declare(strict_types=1);

namespace App\Domain\User\Authentication\DTO;

final readonly class UserAuthTask
{
    public function __construct(
        public string $username,
        public string $plainPassword
    ) {
    }
}
