<?php

declare(strict_types=1);

namespace App\Domain\User\Authentication\Exception;

use App\Domain\Common\DomainException;

final class AuthFailedException extends DomainException
{
    public const string NAME = 'exception.auth.failed';

    public function __construct(
        private readonly string $username,
        private readonly string $plainPassword,
        string $message = '',
        ?\Throwable $previous = null
    ) {
        parent::__construct($message, 0, $previous);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getContext(): array
    {
        return [
            'username' => $this->username,
            'plainPassword' => $this->plainPassword,
        ];
    }
}
