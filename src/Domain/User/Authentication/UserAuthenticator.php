<?php

declare(strict_types=1);

namespace App\Domain\User\Authentication;

use App\Domain\User\Authentication\DTO\UserAuthTask;
use App\Domain\User\Authentication\DTO\UserWithToken;
use App\Domain\User\Authentication\Exception\AuthFailedException;
use App\Domain\User\Password\PasswordProcessor;
use App\Domain\User\Repository\UserRepository;

final readonly class UserAuthenticator
{
    public function __construct(
        private TokenGenerator $tokenGenerator,
        private UserRepository $userRepository,
        private PasswordProcessor $passwordProcessor,
    ) {
    }

    /**
     * @throws AuthFailedException
     */
    public function auth(UserAuthTask $task): UserWithToken
    {
        $user = $this->userRepository->findByUsername($task->username);

        if (null === $user || !$this->passwordProcessor->verify($task->plainPassword, $user->getPassword())) {
            throw new AuthFailedException(
                $task->username,
                $task->plainPassword,
            );
        }

        $token = $this->tokenGenerator->generate($user);

        return new UserWithToken(
            $user,
            $token,
        );
    }
}
