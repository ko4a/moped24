<?php

declare(strict_types=1);

namespace App\Domain\User\Authentication;

use App\Domain\User\Entity\User;

interface TokenGenerator
{
    public function generate(User $user): string;
}
