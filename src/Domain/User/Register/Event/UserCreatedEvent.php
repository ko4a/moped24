<?php

declare(strict_types=1);

namespace App\Domain\User\Register\Event;

use App\Domain\Common\Event;
use App\Domain\User\Entity\User;

final class UserCreatedEvent implements Event
{
    public const string NAME = 'event.domain.user.created';

    public function __construct(public readonly User $user)
    {
    }

    public function getName(): string
    {
        return self::NAME;
    }
}
