<?php

declare(strict_types=1);

namespace App\Domain\User\Register\DTO;

final readonly class RegisterTask
{
    public function __construct(
        public string $username,
        public string $password,
    ) {
    }
}
