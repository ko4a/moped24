<?php

declare(strict_types=1);

namespace App\Domain\User\Register\Exception;

use App\Domain\Common\DomainException;

final class InvalidPasswordException extends DomainException
{
    private const string NAME = 'exception.invalid_password';

    public function __construct(string $message = '', ?\Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getContext(): array
    {
        return [];
    }
}
