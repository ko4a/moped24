<?php

declare(strict_types=1);

namespace App\Domain\User\Register\Exception;

use App\Domain\Common\DomainException;

final class UserAlreadyExistsException extends DomainException
{
    private const string NAME = 'exception.user.already_exists';

    public function __construct(private readonly string $userName, string $message = '', ?\Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @return array<string, mixed>
     */
    public function getContext(): array
    {
        return [
            'userName' => $this->userName,
        ];
    }
}
