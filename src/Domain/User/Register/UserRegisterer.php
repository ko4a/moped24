<?php

declare(strict_types=1);

namespace App\Domain\User\Register;

use App\Domain\Common\DomainException;
use App\Domain\Common\EventDispatcher;
use App\Domain\User\Entity\User;
use App\Domain\User\Password\PasswordProcessor;
use App\Domain\User\Register\DTO\RegisterTask;
use App\Domain\User\Register\Event\UserCreatedEvent;
use App\Domain\User\Register\Exception\UserAlreadyExistsException;
use App\Domain\User\Repository\UserRepository;

final readonly class UserRegisterer
{
    public function __construct(
        private UserRepository $userRepository,
        private PasswordProcessor $passwordProcessor,
        private EventDispatcher $dispatcher,
    ) {
    }

    /**
     * @throws DomainException
     */
    public function register(RegisterTask $task): User
    {
        $user = $this->userRepository->findByUsername($task->username);

        if (null !== $user) {
            throw new UserAlreadyExistsException($user->getUserIdentifier());
        }

        $password = $this->passwordProcessor->hash($task->password);

        $user = new User(
            $task->username,
            $password,
        );

        $this->userRepository->save($user);

        $this->dispatcher->dispatch(new UserCreatedEvent($user));

        return $user;
    }
}
