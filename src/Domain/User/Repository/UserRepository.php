<?php

declare(strict_types=1);

namespace App\Domain\User\Repository;

use App\Domain\Common\EntityNotFoundException;
use App\Domain\User\Entity\User;

interface UserRepository
{
    public function findByUsername(string $username): ?User;

    /**
     * @throws EntityNotFoundException
     */
    public function getByUserName(string $userName): User;

    public function save(User $user): void;

    public function get(int $id): User;
}
