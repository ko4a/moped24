<?php

declare(strict_types=1);

namespace App\Domain\User\Entity;

use App\Domain\Common\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity]
#[ORM\Table(name: 'user_profile')]
#[ORM\HasLifecycleCallbacks]
class User implements UserInterface
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    public readonly int $id;

    public function __construct(
        #[ORM\Column(type: 'string', length: 255, unique: true, nullable: false)]
        private string $username,
        #[ORM\Column(type: 'string', length: 255)]
        private string $password,
    ) {
    }

    /**
     * @return array<string>
     */
    public function getRoles(): array
    {
        return [];
    }

    public function eraseCredentials()
    {
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getUserIdentifier(): string
    {
        return (string)$this->id;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
