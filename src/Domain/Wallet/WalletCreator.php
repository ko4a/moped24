<?php

declare(strict_types=1);

namespace App\Domain\Wallet;

use App\Domain\Wallet\Entity\Wallet;
use App\Domain\Wallet\Exception\WalletAlreadyExistsException;
use App\Domain\Wallet\PaymentManagement\ThirdPartyPaymentManager;
use App\Domain\Wallet\Repository\WalletRepository;
use App\Infrastructure\HttpClient\Clients\NowPayment\Exception\WalletAlreadyExistException;
use App\Infrastructure\HttpClient\Exception\HttpClientException;
use App\Infrastructure\HttpClient\Exception\UnexpectedBehaviourException;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;

final readonly class WalletCreator
{
    public function __construct(
        private ThirdPartyPaymentManager $paymentManager,
        private WalletRepository $walletRepository,
    ) {
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws WalletAlreadyExistsException
     * @throws WalletAlreadyExistException
     * @throws HttpClientException
     * @throws UnexpectedBehaviourException
     */
    public function create(int $userId): Wallet
    {
        $wallet = $this->walletRepository->findByUserId($userId);

        if (null !== $wallet) {
            throw new WalletAlreadyExistsException($userId, 'Wallet already exists');
        }

        $thirdPartyWallet = $this->paymentManager->createWallet($userId);

        $wallet = new Wallet($thirdPartyWallet->id, $userId, '0', '300.00');

        $this->walletRepository->save($wallet);

        return $wallet;
    }
}
