<?php

declare(strict_types=1);

namespace App\Domain\Wallet\Exception;

use App\Domain\Common\DomainException;

final class WalletAlreadyExistsException extends DomainException
{
    private const string NAME = 'wallet.already.exists';

    public function __construct(
        private readonly int $userId,
        string $message = ''
    ) {
        parent::__construct($message);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getContext(): array
    {
        return [
            'userId' => $this->userId,
        ];
    }
}
