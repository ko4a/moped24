<?php

declare(strict_types=1);

namespace App\Domain\Wallet\PaymentManagement;

use App\Domain\Wallet\PaymentManagement\DTO\DepositWithPayment\DepositWithPayment;
use App\Domain\Wallet\PaymentManagement\DTO\DepositWithPayment\DepositWithPaymentTask;
use App\Domain\Wallet\PaymentManagement\DTO\WalletCreatedDto;
use App\Infrastructure\HttpClient\Clients\NowPayment\Exception\WalletAlreadyExistException;
use App\Infrastructure\HttpClient\Exception\HttpClientException;
use App\Infrastructure\HttpClient\Exception\UnexpectedBehaviourException;

interface ThirdPartyPaymentManager
{
    /**
     * @throws WalletAlreadyExistException
     * @throws HttpClientException
     * @throws UnexpectedBehaviourException
     */
    public function createWallet(int $userId): WalletCreatedDto;

    /**
     * @throws HttpClientException
     */
    public function minimumPaymentAmount(string $currency): float;

    public function depositWithPayment(DepositWithPaymentTask $depositWithPaymentTask): DepositWithPayment;
}
