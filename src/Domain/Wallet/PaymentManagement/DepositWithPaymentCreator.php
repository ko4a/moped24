<?php

declare(strict_types=1);

namespace App\Domain\Wallet\PaymentManagement;

use App\Domain\Wallet\Entity\Payment;
use App\Domain\Wallet\PaymentManagement\DTO\DepositWithPayment\DepositWithPaymentTask;
use App\Domain\Wallet\Repository\PaymentRepository;
use App\Domain\Wallet\Repository\WalletRepository;

final readonly class DepositWithPaymentCreator
{
    public function __construct(
        private WalletRepository $walletRepository,
        private ThirdPartyPaymentManager $thirdPartyPaymentManager,
        private PaymentRepository $paymentRepository,
    ) {
    }

    public function create(int $userId, string $currencyCode): Payment
    {
        $wallet = $this->walletRepository->getByUserId($userId);

        $depositWithPayment = $this->thirdPartyPaymentManager->depositWithPayment(new DepositWithPaymentTask(
            $currencyCode,
            $wallet->getExternalId(),
        ));

        return $this->paymentRepository->save(new Payment(
            $depositWithPayment->paymentId,
            $depositWithPayment->paymentStatus,
            $depositWithPayment->payAddress,
            $depositWithPayment->priceCurrency,
            $depositWithPayment->callbackUrl,
            $depositWithPayment->isFeePaidByUser,
            $depositWithPayment->isFixedRate,
            $depositWithPayment->validUntil,
            $depositWithPayment->invoiceId,
        ));
    }
}
