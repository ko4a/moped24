<?php

declare(strict_types=1);

namespace App\Domain\Wallet\PaymentManagement\DTO;

final readonly class WalletCreatedDto
{
    public function __construct(public string $id, public string $name)
    {
    }
}
