<?php

declare(strict_types=1);

namespace App\Domain\Wallet\PaymentManagement\DTO\DepositWithPayment;

class DepositWithPaymentTask
{
    public function __construct(
        public string $currencyCode,
        public string $userExternalId,
    ) {
    }
}
