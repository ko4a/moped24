<?php

declare(strict_types=1);

namespace App\Domain\Wallet\PaymentManagement\DTO\DepositWithPayment;

final readonly class DepositWithPayment
{
    public function __construct(
        public string $paymentId,
        public string $paymentStatus,
        public string $payAddress,
        public string $priceCurrency,
        public string $callbackUrl,
        public bool $isFeePaidByUser,
        public bool $isFixedRate,
        public string $validUntil,
        public string $invoiceId,
    ) {
    }
}
