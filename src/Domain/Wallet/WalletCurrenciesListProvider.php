<?php

declare(strict_types=1);

namespace App\Domain\Wallet;

use App\Domain\Wallet\DTO\Currency;
use App\Infrastructure\HttpClient\Exception\HttpClientException;

interface WalletCurrenciesListProvider
{
    /**
     * @throws HttpClientException
     *
     * @return list<Currency>
     */
    public function list(): array;
}
