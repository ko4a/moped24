<?php

declare(strict_types=1);

namespace App\Domain\Wallet\DTO;

final readonly class Currency
{
    public function __construct(
        public string $externalId,
        public string $code,
        public int $priority,
        public string $logoUrl,
    ) {
    }
}
