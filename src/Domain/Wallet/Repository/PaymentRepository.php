<?php

declare(strict_types=1);

namespace App\Domain\Wallet\Repository;

use App\Domain\Wallet\Entity\Payment;

interface PaymentRepository
{
    public function save(Payment $payment): Payment;
}
