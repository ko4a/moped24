<?php

declare(strict_types=1);

namespace App\Domain\Wallet\Repository;

use App\Domain\Wallet\Entity\Wallet;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;

interface WalletRepository
{
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Wallet $wallet): void;

    public function findByUserId(int $userId): ?Wallet;

    public function getByUserId(int $userId): Wallet;
}
