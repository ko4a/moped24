<?php

declare(strict_types=1);

namespace App\Domain\Wallet\Entity;

use App\Domain\Common\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;

#[ORM\Entity]
#[ORM\Table(name: 'payment')]
#[ORM\HasLifecycleCallbacks]
class Payment
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\Column(type: 'uuid')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    public readonly UuidInterface $uuid;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    // это не amount received - нужно переименовать поле.
    private ?string $amountReceived;

    public function __construct(
        #[ORM\Column(type: 'string', length: 255, nullable: false)]
        private string $externalId,
        #[ORM\Column(type: 'string', length: 64, nullable: false)]
        private string $status,
        #[ORM\Column(type: 'string', length: 128, nullable: false)]
        private string $payAddress,
        #[ORM\Column(type: 'string', length: 64, nullable: false)]
        private string $currency,
        #[ORM\Column(type: 'string', length: 128, nullable: false)]
        private string $callbackUrl,
        #[ORM\Column(type: 'boolean', nullable: false)]
        private bool $isFeePaidByUser,
        #[ORM\Column(type: 'boolean', nullable: false)]
        private bool $isFixedRate,
        #[ORM\Column(type: 'string', length: 128, nullable: false)]
        private string $validUntil,
        #[ORM\Column(type: 'string', length: 255, nullable: false)]
        private string $externalInvoiceId
    ) {
    }

    public function getPayAddress(): string
    {
        return $this->payAddress;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getValidUntil(): string
    {
        return $this->validUntil;
    }

    public function setAmountReceived(string $amountReceived): Payment
    {
        $this->amountReceived = $amountReceived;

        return $this;
    }
}
