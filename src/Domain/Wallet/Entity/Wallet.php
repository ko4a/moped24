<?php

declare(strict_types=1);

namespace App\Domain\Wallet\Entity;

use App\Domain\Common\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'wallet')]
#[ORM\HasLifecycleCallbacks]
class Wallet
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    public readonly int $id;

    public function __construct(
        #[ORM\Column(type: 'string', length: 255, unique: true, nullable: false)]
        private string $externalId,
        #[ORM\Column(type: 'integer', unique: true, nullable: false)]
        private int $userId,
        #[ORM\Column(type: 'string', length: 255, nullable: false)]
        private string $balance,
        #[ORM\Column(type: 'string', length: 255, nullable: false)]
        private string $bonusBalance,
    ) {
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getBalance(): string
    {
        return $this->balance;
    }

    public function getBonusBalance(): string
    {
        return $this->bonusBalance;
    }
}
