<?php

declare(strict_types=1);

namespace App\Domain\Common;

abstract class DomainException extends \Exception
{
    abstract public function getName(): string;

    /**
     * @return array<string, mixed>
     */
    abstract public function getContext(): array;
}
