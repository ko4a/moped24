<?php

declare(strict_types=1);

namespace App\Domain\Common;

interface Event
{
    public const string NAME = '';

    public function getName(): string;
}
