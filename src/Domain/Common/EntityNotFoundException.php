<?php

declare(strict_types=1);

namespace App\Domain\Common;

final class EntityNotFoundException extends DomainException
{
    private const string NAME = 'exception.entity.not_found';

    public function __construct(string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @return array<string, string>
     */
    public function getContext(): array
    {
        return [
            'message' => $this->message,
        ];
    }
}
