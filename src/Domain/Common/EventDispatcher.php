<?php

declare(strict_types=1);

namespace App\Domain\Common;

interface EventDispatcher
{
    public function dispatch(Event $event): void;

    public function clear(): void;

    public function flush(): void;
}
