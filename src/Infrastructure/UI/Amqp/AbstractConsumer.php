<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Amqp;

use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Service\Attribute\Required;

abstract class AbstractConsumer implements ConsumerInterface
{
    protected ?EntityManagerInterface $em = null;
    protected ?SerializerInterface $serializer = null;
    protected ?LoggerInterface $logger = null;

    #[Required]
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    #[Required]
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    #[Required]
    public function setEntityManager(EntityManagerInterface $em): void
    {
        $this->em = $em;
    }

    public function execute(AMQPMessage $msg): int
    {
        try {
            return $this->process(
                $this->serializer->deserialize(
                    $msg->getBody(),
                    $this->getSerializatonMessageFQCN(),
                    'json'
                )
            );
        } catch (\Throwable $exception) {
            $this->logger->alert($exception->getMessage(), $exception->getTrace());

            return self::MSG_ACK;
        }
    }

    abstract protected function process(mixed $message): int;

    abstract protected function getSerializatonMessageFQCN(): string;
}
