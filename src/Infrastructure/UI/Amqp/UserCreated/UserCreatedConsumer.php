<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Amqp\UserCreated;

use App\Application\UseCase\Wallet\Create\WalletCreateUseCase;
use App\Domain\Wallet\Exception\WalletAlreadyExistsException;
use App\Domain\Wallet\Exception\WalletAlreadyExistsException as DomainWalletAlreadyExistsException;
use App\Infrastructure\HttpClient\Exception\HttpClientException;
use App\Infrastructure\HttpClient\Exception\UnexpectedBehaviourException;
use App\Infrastructure\UI\Amqp\AbstractConsumer;
use Doctrine\ORM\Exception\ORMException;
use OldSound\RabbitMqBundle\RabbitMq\Exception\StopConsumerException;

final class UserCreatedConsumer extends AbstractConsumer
{
    public function __construct(private readonly WalletCreateUseCase $walletCreateUseCase)
    {
    }

    protected function process(mixed $message): int
    {
        if (!$message instanceof UserCreatedMessage) {
            return self::MSG_REJECT;
        }

        try {
            $this->walletCreateUseCase->handle($message->userId);
        } catch (DomainWalletAlreadyExistsException|WalletAlreadyExistsException $exception) {
        } catch (HttpClientException|UnexpectedBehaviourException $exception) {
            $this->logger->error($exception->getMessage());
        } catch (ORMException $exception) {
            throw new StopConsumerException();
        }

        return self::MSG_ACK;
    }

    protected function getSerializatonMessageFQCN(): string
    {
        return UserCreatedMessage::class;
    }
}
