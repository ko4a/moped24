<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Amqp\UserCreated;

use Symfony\Component\DependencyInjection\Attribute\Exclude;

#[Exclude]
readonly class UserCreatedMessage
{
    public function __construct(public int $userId)
    {
    }
}
