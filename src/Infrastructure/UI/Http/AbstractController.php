<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Http;

use App\Application\Response\DomainErrorResponse;
use App\Domain\Common\DomainException;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as SymfonyController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Service\Attribute\Required;

class AbstractController extends SymfonyController
{
    private ?SerializerInterface $serializer = null;

    #[Required]
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    protected function unavailable(mixed $responseBody): JsonResponse
    {
        return new JsonResponse(
            $responseBody,
            Response::HTTP_SERVICE_UNAVAILABLE,
            [
                'Retry-after' => '60',
            ],
        );
    }

    /**
     * @param array<string, string> $headers
     * @param array<string, string> $context
     */
    protected function json(mixed $data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        if (null === $this->serializer) {
            throw new \UnexpectedValueException(static::class . ' property serializer is null');
        }

        $data = $this->serializer->serialize($data, 'json');

        return new JsonResponse($data, $status, $headers, true);
    }

    /**
     * @param array<string, string> $headers
     */
    protected function fromDomainException(DomainException $exception, array $headers = []): JsonResponse
    {
        if (null === $this->serializer) {
            throw new \UnexpectedValueException(static::class . ' property serializer is null');
        }

        $domainError = new DomainErrorResponse($exception);

        return new JsonResponse(
            $this->serializer->serialize($domainError, 'json'),
            Response::HTTP_BAD_REQUEST,
            $headers,
            true
        );
    }
}
