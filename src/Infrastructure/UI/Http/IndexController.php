<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Http;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class IndexController extends AbstractController
{
    #[Route('/', name: 'index_action')]
    public function index(): Response
    {
        return $this->redirect('/api/doc');
    }
}
