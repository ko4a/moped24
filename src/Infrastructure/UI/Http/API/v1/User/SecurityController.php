<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Http\API\v1\User;

use App\Application\Response\DomainErrorResponse;
use App\Application\Response\ValidationFailedResponse;
use App\Application\UseCase\User\Auth\AuthUseCase;
use App\Application\UseCase\User\Auth\DTO\AuthRequest;
use App\Application\UseCase\User\Auth\DTO\AuthResponse;
use App\Application\UseCase\User\Register\DTO\RegisterRequest;
use App\Application\UseCase\User\Register\DTO\RegisterResponse;
use App\Application\UseCase\User\Register\RegisterUseCase;
use App\Domain\Common\DomainException;
use App\Domain\User\Authentication\Exception\AuthFailedException;
use App\Infrastructure\UI\Http\AbstractController;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;

final class SecurityController extends AbstractController
{
    public function __construct(
        private readonly AuthUseCase $authUseCase,
        private readonly RegisterUseCase $registerUseCase,
    ) {
    }

    #[Route('/api/v1/security/authenticate/', name: 'auth', methods: ['POST'])]
    #[OA\Post(tags: ['Security'])]
    #[OA\RequestBody(content: new Model(type: AuthRequest::class))]
    #[OA\Response(
        response: AuthResponse::CODE,
        description: 'Returns jwt',
        content: new Model(type: AuthResponse::class)
    )]
    #[OA\Response(
        response: ValidationFailedResponse::CODE,
        description: 'Validation failed',
        content: new Model(type: ValidationFailedResponse::class)
    )]
    #[OA\Response(
        response: DomainErrorResponse::CODE,
        description: AuthFailedException::NAME,
        content: new Model(type: DomainErrorResponse::class)
    )]
    public function authenticate(
        #[MapRequestPayload] AuthRequest $request
    ): Response {
        try {
            $response = $this->authUseCase->handle($request);
        } catch (DomainException $exception) {
            return $this->fromDomainException($exception);
        }

        return $this->json($response, Response::HTTP_OK);
    }

    #[Route('/api/v1/security/register/', name: 'register', methods: ['POST'])]
    #[OA\Post(tags: ['Security'])]
    #[OA\RequestBody(content: new Model(type: RegisterRequest::class))]
    #[OA\Response(
        response: RegisterResponse::CODE,
        description: 'Returns created user',
        content: new Model(type: RegisterResponse::class)
    )]
    #[OA\Response(
        response: ValidationFailedResponse::CODE,
        description: 'Validation failed',
        content: new Model(type: ValidationFailedResponse::class)
    )]
    #[OA\Response(
        response: DomainErrorResponse::CODE,
        description: 'exception.user.already_exists | exception.user.',
        content: new Model(type: DomainErrorResponse::class)
    )]
    public function register(
        #[MapRequestPayload] RegisterRequest $registerRequest,
    ): Response {
        try {
            $response = $this->registerUseCase->handle($registerRequest);
        } catch (DomainException $exception) {
            return $this->fromDomainException($exception);
        }

        return $this->json($response, Response::HTTP_OK);
    }
}
