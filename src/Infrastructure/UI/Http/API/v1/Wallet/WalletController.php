<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Http\API\v1\Wallet;

use App\Application\UseCase\Wallet\Currencies\List\DTO\WalletCurrenciesListResponse;
use App\Application\UseCase\Wallet\Currencies\List\WalletCurrenciesListUseCase;
use App\Application\UseCase\Wallet\GetUserWallet\DTO\GetUserWalletResponse;
use App\Application\UseCase\Wallet\GetUserWallet\GetUserWalletUseCase;
use App\Application\UseCase\Wallet\Payment\Deposit\DepositUseCase;
use App\Application\UseCase\Wallet\Payment\Deposit\DTO\DepositRequest;
use App\Application\UseCase\Wallet\Payment\Deposit\DTO\DepositResponse;
use App\Application\UseCase\Wallet\Payment\MinimumAmount\DTO\MinimumPaymentAmountRequest;
use App\Application\UseCase\Wallet\Payment\MinimumAmount\DTO\MinimumPaymentAmountResponse;
use App\Application\UseCase\Wallet\Payment\MinimumAmount\MinimumPaymentAmountUseCase;
use App\Domain\User\Entity\User;
use App\Infrastructure\HttpClient\Clients\NowPayment\Exception\CurrencyNotExistsException;
use App\Infrastructure\HttpClient\Exception\HttpClientException;
use App\Infrastructure\UI\Http\AbstractController;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

final class WalletController extends AbstractController
{
    public function __construct(
        private readonly WalletCurrenciesListUseCase $walletCurrenciesListUseCase,
        private readonly MinimumPaymentAmountUseCase $minimumPaymentAmountUseCase,
        private readonly GetUserWalletUseCase $userWalletUseCase,
        private readonly DepositUseCase $depositUseCase,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route('/api/v1/wallet/payment/deposit/', name: 'wallet_deposit_post', methods: ['POST'])]
    #[OA\Post(tags: ['Wallet'])]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'deposit payment',
        content: new Model(type: DepositResponse::class)
    )]
    #[OA\Response(
        response: Response::HTTP_SERVICE_UNAVAILABLE,
        description: 'In case if smth wrong with payment api connection',
    )]
    public function deposit(
        #[MapRequestPayload] DepositRequest $request,
        #[CurrentUser] User $currentUser
    ): Response {
        try {
            $response = $this->depositUseCase->handle($request, $currentUser->id);
        } catch (HttpClientException $exception) {
            $this->logger->error($exception->getMessage());

            return $this->unavailable(['text' => 'Try again later']);
        }

        return $this->json($response, Response::HTTP_CREATED);
    }

    #[Route('/api/v1/wallet/', name: 'wallet_get', methods: ['GET'])]
    #[OA\Get(tags: ['Wallet'])]
    #[OA\Response(
        response: GetUserWalletResponse::CODE,
        description: 'user wallet',
        content: new Model(type: GetUserWalletResponse::class)
    )]
    #[OA\Response(
        response: Response::HTTP_SERVICE_UNAVAILABLE,
        description: 'In case if smth wrong with payment api connection',
    )]
    public function getMyWallet(
        #[CurrentUser] User $currentUser
    ): Response {
        try {
            $response = $this->userWalletUseCase->handle($currentUser->id);
        } catch (\Throwable $e) {
            $this->logger->alert('Erorr get wallet', ['exception' => $e->getMessage()]);

            return $this->unavailable(['text' => 'Try again later']);
        }

        return $this->json($response, Response::HTTP_OK);
    }

    #[Route('/api/v1/wallet/currencies/', name: 'wallet_currencies_list', methods: ['GET'])]
    #[OA\Get(tags: ['Wallet'])]
    #[OA\Response(
        response: WalletCurrenciesListResponse::CODE,
        description: 'currencies',
        content: new Model(type: WalletCurrenciesListResponse::class)
    )]
    #[OA\Response(
        response: Response::HTTP_SERVICE_UNAVAILABLE,
        description: 'In case if smth wrong with payment api connection',
    )]
    public function listAvailableCurrencies(): Response
    {
        try {
            $response = $this->walletCurrenciesListUseCase->handle();
        } catch (HttpClientException $e) {
            $this->logger->alert('Erorr durring list available currencies', ['exception' => $e->getMessage()]);

            return $this->unavailable(['text' => 'Try again later']);
        }

        return $this->json($response, Response::HTTP_OK);
    }

    #[Route('/api/v1/wallet/payment/minimum/', name: 'wallet_min_payment_amount', methods: ['GET'])]
    #[OA\Get(tags: ['Wallet'])]
    #[OA\Response(
        response: MinimumPaymentAmountResponse::CODE,
        description: 'minimum payment amount for currency',
        content: new Model(type: MinimumPaymentAmountResponse::class)
    )]
    #[OA\Response(
        response: Response::HTTP_SERVICE_UNAVAILABLE,
        description: 'In case if smth wrong with payment api connection',
    )]
    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'In case if currency not exists',
    )]
    public function getMinimumPaymentAmountForCurrency(
        #[MapQueryString] MinimumPaymentAmountRequest $request
    ): Response {
        try {
            $response = $this->minimumPaymentAmountUseCase->handle($request);
        } catch (CurrencyNotExistsException $e) {
            return $this->json(['message' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        } catch (HttpClientException $e) {
            $this->logger->alert('Erorr durring get min payment amount', ['exception' => $e->getMessage()]);

            return $this->unavailable(['text' => 'Try again later']);
        }

        return $this->json($response, Response::HTTP_OK);
    }
}
