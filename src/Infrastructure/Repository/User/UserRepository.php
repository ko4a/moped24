<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\User;

use App\Domain\Common\EntityNotFoundException;
use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepository as UserRegisterRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * @extends EntityRepository<User>
 */
final class UserRepository extends EntityRepository implements UserRegisterRepositoryInterface
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getClassMetadata(User::class));
    }

    /**
     * @throws EntityNotFoundException
     */
    public function getByUserName(string $userName): User
    {
        $user = $this->findByUsername($userName);

        if (null === $user) {
            throw new EntityNotFoundException(sprintf('User with username "%s" does not exist.', $userName));
        }

        return $user;
    }

    public function findByUsername(string $userName): ?User
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        return $qb
            ->select('user')
            ->from(User::class, 'user')
            ->andWhere($qb->expr()->eq('user.username', ':username'))
            ->setParameter('username', $userName)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function save(User $user): void
    {
        $this->getEntityManager()->persist($user);
    }

    /**
     * @throws EntityNotFoundException
     */
    public function get(int $id): User
    {
        $user = $this->getEntityManager()->find(User::class, $id);

        if (null === $user) {
            throw new EntityNotFoundException(
                sprintf('User with id %d not found', $id)
            );
        }

        return $user;
    }
}
