<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Wallet;

use App\Domain\Common\EntityNotFoundException;
use App\Domain\Wallet\Entity\Wallet;
use App\Domain\Wallet\Repository\WalletRepository as DomainWalletRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;

/**
 * @extends EntityRepository<Wallet>
 */
class WalletRepository extends EntityRepository implements DomainWalletRepository
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getClassMetadata(Wallet::class));
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Wallet $wallet): void
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getEntityManager();

        $entityManager->persist($wallet);
        $entityManager->flush();
    }

    public function getByUserId(int $userId): Wallet
    {
        $wallet = $this->findByUserId($userId);

        if (null === $wallet) {
            throw new EntityNotFoundException(sprintf('Wallet with user id "%s" does not exist.', $userId));
        }

        return $wallet;
    }

    public function findByUserId(int $userId): ?Wallet
    {
        return $this->findOneBy(['userId' => $userId]);
    }
}
