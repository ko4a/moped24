<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Wallet;

use App\Domain\Wallet\Entity\Payment;
use App\Domain\Wallet\Repository\PaymentRepository as DomainPaymentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * @extends EntityRepository<Payment>
 */
class PaymentRepository extends EntityRepository implements DomainPaymentRepository
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getClassMetadata(Payment::class));
    }

    public function save(Payment $payment): Payment
    {
        $this->getEntityManager()->persist($payment);
        $this->getEntityManager()->flush();

        return $payment;
    }
}
