<?php

declare(strict_types=1);

namespace App\Infrastructure\Events;

use Symfony\Contracts\EventDispatcher\Event as SymfonyEvent;

class Event extends SymfonyEvent
{
}
