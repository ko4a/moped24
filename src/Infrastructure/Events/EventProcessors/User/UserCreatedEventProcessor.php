<?php

declare(strict_types=1);

namespace App\Infrastructure\Events\EventProcessors\User;

use App\Domain\User\Register\Event\UserCreatedEvent;
use App\Infrastructure\Events\AbstractEventProcessor;
use App\Infrastructure\UI\Amqp\UserCreated\UserCreatedMessage;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(event: UserCreatedEvent::NAME, method: 'onUserCreated')]
final readonly class UserCreatedEventProcessor extends AbstractEventProcessor
{
    public function onUserCreated(UserCreatedEvent $event): void
    {
        $this->pushToQueue(new UserCreatedMessage($event->user->id));
    }
}
