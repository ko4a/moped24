<?php

declare(strict_types=1);

namespace App\Infrastructure\Events;

use App\Domain\Common\Event;
use App\Domain\Common\EventDispatcher as EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface as SymfonyEventDispatcher;

final class EventDispatcher implements EventDispatcherInterface
{
    /**
     * @var array<int, Event>
     */
    private array $events = [];

    public function __construct(private readonly SymfonyEventDispatcher $symfonyEventDispatcher)
    {
    }

    public function dispatch(Event $event): void
    {
        $this->events[] = $event;
    }

    public function clear(): void
    {
        $this->events = [];
    }

    public function flush(): void
    {
        foreach ($this->events as $event) {
            $this->symfonyEventDispatcher->dispatch($event, $event->getName());
        }
    }
}
