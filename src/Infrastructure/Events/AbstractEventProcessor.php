<?php

declare(strict_types=1);

namespace App\Infrastructure\Events;

use JMS\Serializer\SerializerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;

abstract readonly class AbstractEventProcessor
{
    public function __construct(
        private ProducerInterface $producer,
        private SerializerInterface $serializer
    ) {
    }

    public function pushToQueue(mixed $message): void
    {
        $this->producer->publish(
            $this->serializer->serialize($message, 'json')
        );
    }
}
