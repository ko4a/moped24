<?php

declare(strict_types=1);

namespace App\Infrastructure\Events;

use App\Application\Response\AccessDeniedResponse;
use App\Application\Response\ValidationFailedResponse;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Validator\Exception\ValidationFailedException;

final class UnexpectedExceptionSubscriber implements EventSubscriberInterface
{
    private const string INVALID = 'exception.unexpected';

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly SerializerInterface $serializer
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['onKernelException', 0],
            ],
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $ex = $event->getThrowable();

        if ($ex instanceof HttpException && $ex->getPrevious() instanceof ValidationFailedException) {
            $this->validationFailedResponse($event);

            return;
        }

        if ($ex instanceof AccessDeniedHttpException) {
            $this->accessDeniedResponse($event);

            return;
        }

        if ($ex instanceof MethodNotAllowedHttpException) {
            $event->setResponse(
                new JsonResponse(
                    ['name' => 'exception.method_not_allowed'],
                    Response::HTTP_METHOD_NOT_ALLOWED,
                )
            );

            return;
        }

        $this->logger->alert(
            sprintf('Unexpected exception: %s', $ex->getMessage()),
            $ex->getTrace(),
        );

        $event->setResponse(
            new JsonResponse(['name' => self::INVALID], Response::HTTP_INTERNAL_SERVER_ERROR)
        );
    }

    private function accessDeniedResponse(ExceptionEvent $event): void
    {
        $response = new AccessDeniedResponse(
            $event->getThrowable()->getMessage()
        );

        $event->setResponse(new JsonResponse(
            $this->serializer->serialize($response, 'json'),
            Response::HTTP_FORBIDDEN,
            [],
            true
        ));
    }

    private function validationFailedResponse(ExceptionEvent $event): void
    {
        $response = new ValidationFailedResponse(
            $event->getThrowable()->getMessage()
        );

        $event->setResponse(new JsonResponse(
            $this->serializer->serialize($response, 'json'),
            ValidationFailedResponse::CODE,
            [],
            true
        )
        );
    }
}
