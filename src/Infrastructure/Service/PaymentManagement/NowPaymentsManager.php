<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\PaymentManagement;

use App\Domain\Wallet\DTO\Currency;
use App\Domain\Wallet\PaymentManagement\DTO\DepositWithPayment\DepositWithPayment;
use App\Domain\Wallet\PaymentManagement\DTO\DepositWithPayment\DepositWithPaymentTask;
use App\Domain\Wallet\PaymentManagement\DTO\WalletCreatedDto;
use App\Domain\Wallet\PaymentManagement\ThirdPartyPaymentManager;
use App\Domain\Wallet\WalletCurrenciesListProvider;
use App\Infrastructure\HttpClient\Clients\NowPayment\Exception\WalletAlreadyExistException;
use App\Infrastructure\HttpClient\Clients\NowPayment\NowPaymentClient;
use App\Infrastructure\HttpClient\Exception\HttpClientException;
use App\Infrastructure\HttpClient\Exception\UnexpectedBehaviourException;
use Psr\Cache\CacheItemPoolInterface;

final readonly class NowPaymentsManager implements ThirdPartyPaymentManager, WalletCurrenciesListProvider
{
    private const string JWT_TOKEN_CACHE_KEY = 'now_payments_jwt_token';
    private const string AVAILABLE_CURRENCIES_CACHE_KEY = 'now_payments_available_currencies';
    private const int JWT_TOKEN_CACHE_TTL = 60 * 4;
    private const int AVAILABLE_CURRENCIES_TTL = 60 * 5;

    private const float MINIMUM_DEPOSIT_MULTIPLIER = 1.5;

    public function __construct(
        private NowPaymentClient $client,
        private CacheItemPoolInterface $cachePool,
        private string $depositWithPaymentCallback,
    ) {
    }

    /**
     * @throws HttpClientException
     */
    private function authenticate(): string
    {
        $jwtCacheItem = $this->cachePool->getItem(self::JWT_TOKEN_CACHE_KEY);

        if ($jwtCacheItem->isHit()) {
            return (string)$jwtCacheItem->get();
        }

        $jwtToken = $this->client->jwtAuthenticate()->token;

        $jwtCacheItem->expiresAfter(self::JWT_TOKEN_CACHE_TTL);
        $jwtCacheItem->set($jwtToken);
        $this->cachePool->save($jwtCacheItem);

        return $jwtToken;
    }

    /**
     * @throws WalletAlreadyExistException
     * @throws HttpClientException
     * @throws UnexpectedBehaviourException
     */
    public function createWallet(int $userId): WalletCreatedDto
    {
        $jwtToken = $this->authenticate();

        $nowPaymentWallet = $this->client->createWallet($userId, $jwtToken);

        return new WalletCreatedDto($nowPaymentWallet->result->id, $nowPaymentWallet->result->name);
    }

    /**
     * @throws HttpClientException
     */
    public function list(): array
    {
        $currenciesCacheItem = $this->cachePool->getItem(self::AVAILABLE_CURRENCIES_CACHE_KEY);

        if ($currenciesCacheItem->isHit()) {
            return (array)$currenciesCacheItem->get();
        }

        $currencies = $this->client->listCurrencies();
        usort($currencies, static fn (Currency $a, Currency $b): int => $a->priority <=> $b->priority);

        $currenciesCacheItem->expiresAfter(self::AVAILABLE_CURRENCIES_TTL);
        $currenciesCacheItem->set($currencies);
        $this->cachePool->save($currenciesCacheItem);

        return $currencies;
    }

    /**
     * @throws HttpClientException
     */
    public function minimumPaymentAmount(string $currency): float
    {
        return $this->client->minimumPaymentAmount($currency) * self::MINIMUM_DEPOSIT_MULTIPLIER;
    }

    /**
     * @throws HttpClientException
     */
    public function depositWithPayment(DepositWithPaymentTask $depositWithPaymentTask): DepositWithPayment
    {
        $jwt = $this->authenticate();

        $minimumAmount = $this->minimumPaymentAmount($depositWithPaymentTask->currencyCode);

        return $this->client->depositWithPayment(
            $jwt,
            $depositWithPaymentTask->currencyCode,
            $minimumAmount,
            $depositWithPaymentTask->userExternalId,
            $this->depositWithPaymentCallback,
        );
    }
}
