<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Security;

use App\Domain\User\Authentication\TokenGenerator;
use App\Domain\User\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

final readonly class JwtTokenGenerator implements TokenGenerator
{
    public function __construct(private JWTTokenManagerInterface $jwtTokenManager)
    {
    }

    public function generate(User $user): string
    {
        return $this->jwtTokenManager->create($user);
    }
}
