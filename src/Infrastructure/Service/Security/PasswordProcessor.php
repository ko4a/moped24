<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Security;

use App\Domain\User\Password\PasswordProcessor as DomainPasswordProcessor;
use App\Domain\User\Register\Exception\InvalidPasswordException;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

final readonly class PasswordProcessor implements DomainPasswordProcessor
{
    public function __construct(private PasswordHasherInterface $passwordHasher)
    {
    }

    /**
     * @throws InvalidPasswordException
     */
    public function hash(string $plainPassword): string
    {
        try {
            return $this->passwordHasher->hash($plainPassword);
        } catch (\Throwable $exception) {
            throw new InvalidPasswordException('Invalid password', $exception);
        }
    }

    public function verify(string $plainPassword, string $hashedPassword): bool
    {
        return $this->passwordHasher->verify($hashedPassword, $plainPassword);
    }
}
