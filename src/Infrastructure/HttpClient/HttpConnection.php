<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient;

use App\Infrastructure\HttpClient\Builder\RequestBuilder;

interface HttpConnection
{
    public function createBuilder(): RequestBuilder;
}
