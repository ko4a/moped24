<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient;

final class TimeoutResolver
{
    /**
     * @param array<string,array<string, array<string, int>>> $timeouts
     */
    public function __construct(
        private readonly array $timeouts
    ) {
    }

    public function resolve(string $method, string $baseUrl, string $url): float
    {
        $requestTimeouts = $this->timeouts[$baseUrl] ?? [];

        foreach ($requestTimeouts as $path => $timeout) {
            /* @noinspection PhpComposerExtensionStubsInspection */
            if (mb_ereg_match($path, $url)) {
                $requestTimeout = is_array($timeout) ? ($timeout[$method] ?? 0) : $timeout;

                return (float)$requestTimeout;
            }
        }

        return 0;
    }
}
