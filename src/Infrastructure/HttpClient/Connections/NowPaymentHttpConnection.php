<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Connections;

use App\Infrastructure\HttpClient\Builder\BodyParams\JsonBodyParamsCompiler;
use App\Infrastructure\HttpClient\Builder\RequestBuilder;
use App\Infrastructure\HttpClient\Exception\NetworkException;
use App\Infrastructure\HttpClient\Exception\NoResponseException;
use App\Infrastructure\HttpClient\Exception\UnexpectedBehaviourException;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final readonly class NowPaymentHttpConnection implements HttpConnection
{
    public function __construct(
        private string $baseUrl,
        private string $authenticationEmail,
        private string $authenticationPassword,
        private string $apiKey,
        private ClientInterface $client,
    ) {
    }

    public function createRequestBuilder(): RequestBuilder
    {
        $builder = new RequestBuilder(new Uri($this->baseUrl));
        $builder
            ->addHeader('Content-Type', 'application/json')
            ->setBodyParamsCompiler(new JsonBodyParamsCompiler())
        ;

        return $builder;
    }

    /**
     * @throws NetworkException
     * @throws NoResponseException
     * @throws UnexpectedBehaviourException
     */
    public function send(RequestInterface $request): ResponseInterface
    {
        $contentLength = mb_strlen($request->getBody()->getContents(), '8bit');
        $request->getBody()->rewind();

        /** @phpstan-ignore-next-line */
        $request = $request->withAddedHeader('Content-Length', $contentLength);

        return $this->client->sendRequest($request);
    }

    public function getAuthenticationEmail(): string
    {
        return $this->authenticationEmail;
    }

    public function getAuthenticationPassword(): string
    {
        return $this->authenticationPassword;
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }
}
