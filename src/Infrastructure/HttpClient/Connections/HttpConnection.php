<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Connections;

use App\Infrastructure\HttpClient\Builder\RequestBuilder;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface HttpConnection
{
    public function createRequestBuilder(): RequestBuilder;

    public function send(RequestInterface $request): ResponseInterface;
}
