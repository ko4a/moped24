<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Clients\NowPayment\DTO\WalletCreated;

final readonly class WalletCreatedResult
{
    public function __construct(
        public string $id,
        public string $name,
    ) {
    }
}
