<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Clients\NowPayment\DTO\WalletCreated;

final readonly class WalletCreatedResponse
{
    public function __construct(public WalletCreatedResult $result)
    {
    }
}
