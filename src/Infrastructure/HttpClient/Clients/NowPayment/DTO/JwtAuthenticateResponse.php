<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Clients\NowPayment\DTO;

final readonly class JwtAuthenticateResponse
{
    public function __construct(public string $token)
    {
    }
}
