<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Clients\NowPayment\Exception;

use App\Infrastructure\HttpClient\Exception\HttpClientException;

final class DepositException extends HttpClientException
{
}
