<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Clients\NowPayment;

use App\Domain\Wallet\DTO\Currency;
use App\Domain\Wallet\PaymentManagement\DTO\DepositWithPayment\DepositWithPayment;
use App\Infrastructure\HttpClient\Clients\NowPayment\DTO\JwtAuthenticateResponse;
use App\Infrastructure\HttpClient\Clients\NowPayment\DTO\WalletCreated\WalletCreatedResponse;
use App\Infrastructure\HttpClient\Clients\NowPayment\DTO\WalletCreated\WalletCreatedResult;
use App\Infrastructure\HttpClient\Clients\NowPayment\Exception\CurrencyNotExistsException;
use App\Infrastructure\HttpClient\Clients\NowPayment\Exception\DepositException;
use App\Infrastructure\HttpClient\Clients\NowPayment\Exception\WalletAlreadyExistException;
use App\Infrastructure\HttpClient\Connections\NowPaymentHttpConnection;
use App\Infrastructure\HttpClient\Exception\HttpClientException;
use App\Infrastructure\HttpClient\Exception\UnexpectedBehaviourException;

final readonly class NowPaymentClient
{
    private const string POST_CREATE_USER_WALLET = '/v1/sub-partner/balance';
    private const string POST_AUTH_JWT = '/v1/auth';
    private const string GET_LIST_CURRENCIES = '/v1/full-currencies';
    private const string POST_DEPOSIT_WITH_PAYMENT = '/v1/sub-partner/payment';
    private const string GET_MINIMUM_PAYMENT_AMOUNT = '/v1/min-amount';

    public function __construct(private NowPaymentHttpConnection $connection)
    {
    }

    /**
     * @throws HttpClientException
     */
    public function jwtAuthenticate(): JwtAuthenticateResponse
    {
        $builder = $this->connection->createRequestBuilder();

        $request = $builder
            ->post(self::POST_AUTH_JWT)
            ->addBodyParams(
                [
                    'email' => $this->connection->getAuthenticationEmail(),
                    'password' => $this->connection->getAuthenticationPassword(),
                ]
            )
            ->build()
            ->toPsrRequest()
        ;

        $response = $this->connection->send($request);

        if (200 !== $response->getStatusCode()) {
            throw new HttpClientException($request, 'NowPayment authentication failed');
        }

        $body = json_decode($response->getBody()->getContents(), true);

        if (empty($body['token'])) {
            throw new UnexpectedBehaviourException($request, 'There is no token in NowPayment auth response');
        }

        return new JwtAuthenticateResponse((string)$body['token']);
    }

    /**
     * @throws UnexpectedBehaviourException
     * @throws WalletAlreadyExistException
     * @throws HttpClientException
     */
    public function createWallet(int $userId, string $jwtToken): WalletCreatedResponse
    {
        $builder = $this->connection->createRequestBuilder();

        $request = $builder
            ->post(self::POST_CREATE_USER_WALLET)
            ->bearer($jwtToken)
            ->addBodyParam('name', (string)$userId)
            ->build()
            ->toPsrRequest()
        ;

        $response = $this->connection->send($request);

        if (200 === $response->getStatusCode()) {
            $json = json_decode($response->getBody()->getContents(), true);

            return new WalletCreatedResponse(
                new WalletCreatedResult(
                    $json['result']['id'],
                    $json['result']['name'],
                )
            );
        }

        if (400 === $response->getStatusCode()) {
            $json = json_decode($response->getBody()->getContents(), true);

            throw new WalletAlreadyExistException($request, $json['message'] ?? '');
        }

        throw new UnexpectedBehaviourException($request, 'NowPayment create wallet failed');
    }

    /**
     * @throws HttpClientException
     *
     * @return list<Currency>
     */
    public function listCurrencies(): array
    {
        $builder = $this->connection->createRequestBuilder();

        $request = $builder
            ->get(self::GET_LIST_CURRENCIES)
            ->addHeader('x-api-key', $this->connection->getApiKey())
            ->build()
            ->toPsrRequest()
        ;

        $response = $this->connection->send($request);

        if (200 !== $response->getStatusCode()) {
            throw new UnexpectedBehaviourException($request, 'NowPayment list currencies failed');
        }

        $json = json_decode($response->getBody()->getContents(), true);

        if (empty($json['currencies'])) {
            throw new UnexpectedBehaviourException($request, 'No currencies in api response');
        }

        $result = [];

        foreach ($json['currencies'] as $currency) {
            if (true !== $currency['enable']) {
                continue;
            }

            $result[] = new Currency(
                (string)$currency['id'],
                $currency['code'],
                $currency['priority'],
                $currency['logo_url'],
            );
        }

        return $result;
    }

    /**
     * @throws HttpClientException
     */
    public function minimumPaymentAmount(string $currency): float
    {
        $builder = $this->connection->createRequestBuilder();

        $request = $builder
            ->get(self::GET_MINIMUM_PAYMENT_AMOUNT)
            ->addHeader('x-api-key', $this->connection->getApiKey())
            ->addQueryParam('currency_from', $currency)
            ->addQueryParam('is_fee_paid_by_user', 'false')
            ->build()
            ->toPsrRequest()
        ;

        $response = $this->connection->send($request);

        if (404 === $response->getStatusCode()) {
            throw new CurrencyNotExistsException($request, sprintf('Currency %s not found', $currency));
        }

        if (200 !== $response->getStatusCode()) {
            throw new UnexpectedBehaviourException($request, 'NowPayment minimum payment amount failed');
        }

        $json = json_decode($response->getBody()->getContents(), true);

        return (float)$json['min_amount'];
    }

    /**
     * @throws HttpClientException
     */
    public function depositWithPayment(
        string $jwtToken,
        string $currencyCode,
        float $amount,
        string $userExternalId,
        string $ipnCallbackUrl
    ): DepositWithPayment {
        $builder = $this->connection->createRequestBuilder();

        $request = $builder
            ->post(self::POST_DEPOSIT_WITH_PAYMENT)
            ->addHeader('x-api-key', $this->connection->getApiKey())
            ->bearer($jwtToken)
            ->addBodyParam('currency', $currencyCode)
            ->addBodyParam('amount', (string)$amount)
            ->addBodyParam('sub_partner_id', $userExternalId)
            ->addBodyParam('is_fixed_rate', false)
            ->addBodyParam('is_fee_paid_by_user', false)
            ->addBodyParam('ipn_callback_url', $ipnCallbackUrl)
            ->build()
            ->toPsrRequest();

        $response = $this->connection->send($request);

        if (200 !== $response->getStatusCode()) {
            throw new DepositException(
                $request,
                $response->getBody()->getContents(),
            );
        }

        $responseBody = json_decode($response->getBody()->getContents(), true);

        return new DepositWithPayment(
            $responseBody['result']['payment_id'],
            $responseBody['result']['payment_status'],
            $responseBody['result']['pay_address'],
            $responseBody['result']['price_currency'],
            $responseBody['result']['ipn_callback_url'],
            $responseBody['result']['is_fee_paid_by_user'],
            $responseBody['result']['is_fixed_rate'],
            $responseBody['result']['valid_until'],
            $responseBody['result']['invoice_id'],
        );
    }
}
