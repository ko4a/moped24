<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Exception;

use Psr\Http\Message\RequestInterface;

class HttpClientException extends \Exception
{
    public function __construct(
        public readonly RequestInterface $request,
        string $message = '',
        ?\Throwable $previous = null
    ) {
        parent::__construct($message, 0, $previous);
    }
}
