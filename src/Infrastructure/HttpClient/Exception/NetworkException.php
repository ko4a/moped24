<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Exception;

final class NetworkException extends HttpClientException
{
}
