<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Exception;

final class NoResponseException extends HttpClientException
{
}
