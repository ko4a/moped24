<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient;

use App\Infrastructure\HttpClient\Exception\NetworkException;
use App\Infrastructure\HttpClient\Exception\NoResponseException;
use App\Infrastructure\HttpClient\Exception\UnexpectedBehaviourException;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException as GuzzleRequestException;
use GuzzleHttp\RequestOptions;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final readonly class GuzzleClientDecorator implements ClientInterface
{
    public function __construct(
        private GuzzleClientInterface $guzzle,
        private TimeoutResolver $timeoutResolver
    ) {
    }

    /**
     * @throws NetworkException
     * @throws NoResponseException
     * @throws UnexpectedBehaviourException
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        try {
            return $this->guzzle->send($request, $this->getOptions($request));
        } catch (ConnectException $e) {
            throw new NetworkException(
                $e->getRequest(),
                'Could not connect to ' . $request->getUri(),
                $e,
            );
        } catch (GuzzleRequestException $e) {
            if ($e->hasResponse()) {
                return $e->getResponse();
            }

            throw new NoResponseException(
                $e->getRequest(),
                'No response returned for ' . $request->getUri(),
                $e,
            );
        } catch (\Throwable $e) {
            throw new UnexpectedBehaviourException(
                $request,
                'Something went wrong while send request to ' . $request->getUri(),
                $e,
            );
        }
    }

    /**
     * @return array<string, float>
     */
    private function getOptions(RequestInterface $request): array
    {
        $baseUrl = "{$request->getUri()->getScheme()}://{$request->getUri()->getHost()}";

        return [RequestOptions::TIMEOUT => $this->timeoutResolver->resolve(
            $request->getMethod(), $baseUrl, $request->getUri()->getPath()
        )];
    }
}
