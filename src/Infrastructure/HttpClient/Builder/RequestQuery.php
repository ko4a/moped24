<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Builder;

use App\Infrastructure\HttpClient\Builder\BodyParams\BodyParamsCompiler;
use GuzzleHttp\Psr7\Query;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;

final class RequestQuery
{
    /**
     * @param array<string, mixed> $headers
     * @param array<string, mixed> $queryParams
     * @param array<string, mixed> $bodyParams
     */
    public function __construct(
        private string $method,
        private Uri $uri,
        private array $headers,
        private array $queryParams,
        private array $bodyParams,
        private ?BodyParamsCompiler $bodyParamsCompiler,
    ) {
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getUri(): Uri
    {
        return $this->uri;
    }

    /**
     * @return array<string, mixed>
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return array<string, mixed>
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * @return array<string, mixed>
     */
    public function getBodyParams(): array
    {
        return $this->bodyParams;
    }

    public function toPsrRequest(): RequestInterface
    {
        return new Request(
            $this->method,
            $this->compileUri(),
            $this->headers,
            $this->compileBody()
        );
    }

    private function compileUri(): Uri
    {
        $uri = $this->uri;
        if (!empty($this->queryParams)) {
            $uri = $uri->withQuery(Query::build($this->queryParams));
        }

        return $uri;
    }

    private function compileBody(): ?string
    {
        if (empty($this->bodyParams)) {
            return null;
        }

        if (null === $this->bodyParamsCompiler) {
            throw new \InvalidArgumentException('bodyParamsCompiler must be set');
        }

        return $this->bodyParamsCompiler->compile($this->bodyParams);
    }
}
