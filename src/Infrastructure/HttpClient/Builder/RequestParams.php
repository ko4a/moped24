<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Builder;

final class RequestParams
{
    /**
     * @var array<string, mixed>
     */
    private array $params = [];

    public function add(string $name, mixed $value): void
    {
        if (isset($this->params[$name])) {
            throw new \InvalidArgumentException(sprintf('param %s already set', $name));
        }

        $this->params[$name] = $value;
    }

    /**
     * @param array<string, mixed> $params
     */
    public function addParams(array $params): void
    {
        foreach ($params as $name => $value) {
            $this->add($name, $value);
        }
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return $this->params;
    }

    public function isEmpty(): bool
    {
        return empty($this->params);
    }
}
