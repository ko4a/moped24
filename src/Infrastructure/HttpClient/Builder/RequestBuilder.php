<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Builder;

use App\Infrastructure\HttpClient\Builder\BodyParams\BodyParamsCompiler;
use GuzzleHttp\Psr7\Uri;

class RequestBuilder
{
    private RequestParams $queryParams;

    private RequestParams $headers;
    private RequestParams $bodyParams;

    private ?BodyParamsCompiler $bodyParamsCompiler = null;

    private string $method;

    public function __construct(
        private Uri $uri
    ) {
        $this->queryParams = new RequestParams();
        $this->headers = new RequestParams();
        $this->bodyParams = new RequestParams();
    }

    /**
     * Добавляет параметры в запрос, параметры не могут быть перезаписан.
     *
     * @param array<string, mixed> $params
     */
    public function addQueryParams(array $params): self
    {
        $this->queryParams->addParams($params);

        return $this;
    }

    /**
     * Добавляет параметры в запрос, параметры не могут быть перезаписан.
     */
    public function addQueryParam(string $name, string $value): self
    {
        $this->queryParams->add($name, $value);

        return $this;
    }

    public function setPath(string $path): self
    {
        $this->uri = $this->uri->withPath('/' . ltrim($path, '/'));

        return $this;
    }

    public function addHeader(string $name, string|int $value): self
    {
        $this->headers->add($name, $value);

        return $this;
    }

    /**
     * Добавляет параметры в боди, параметры не могут быть перезаписан.
     *
     * @param array<string, mixed> $params
     */
    public function addBodyParams(array $params): self
    {
        $this->bodyParams->addParams($params);

        return $this;
    }

    public function addBodyParam(string $name, mixed $value): self
    {
        $this->bodyParams->add($name, $value);

        return $this;
    }

    public function setBodyParamsCompiler(BodyParamsCompiler $compiler): self
    {
        $this->bodyParamsCompiler = $compiler;

        return $this;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function get(string $path): self
    {
        return $this->setMethod('get')->setPath($path);
    }

    public function post(string $path): self
    {
        return $this->setMethod('post')->setPath($path);
    }

    public function bearer(string $token): self
    {
        return $this->addHeader('Authorization', 'Bearer ' . $token);
    }

    public function build(): RequestQuery
    {
        return new RequestQuery(
            $this->method,
            $this->uri,
            $this->headers->toArray(),
            $this->queryParams->toArray(),
            $this->bodyParams->toArray(),
            $this->bodyParamsCompiler,
        );
    }
}
