<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Builder\BodyParams;

use Symfony\Component\DependencyInjection\Attribute\Exclude;

#[Exclude]
final class JsonBodyParamsCompiler implements BodyParamsCompiler
{
    /**
     * @throws \JsonException
     */
    public function compile(array $params): string
    {
        return json_encode($params, JSON_THROW_ON_ERROR);
    }
}
