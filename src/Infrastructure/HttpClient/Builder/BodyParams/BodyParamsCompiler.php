<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpClient\Builder\BodyParams;

interface BodyParamsCompiler
{
    /**
     * @param array<string, mixed> $params
     */
    public function compile(array $params): string;
}
