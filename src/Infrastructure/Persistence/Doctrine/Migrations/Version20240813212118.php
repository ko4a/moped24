<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240813212118 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'creates payment';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE payment_uuid_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE payment (
            uuid UUID NOT NULL,
            amount_received VARCHAR(255) DEFAULT NULL,
            external_invoice_id VARCHAR(255) NOT NULL,
            external_id VARCHAR(255) NOT NULL,
            status VARCHAR(64) NOT NULL,
            pay_address VARCHAR(128) NOT NULL,
            currency VARCHAR(64) NOT NULL,
            callback_url VARCHAR(128) NOT NULL,
            is_fee_paid_by_user BOOLEAN NOT NULL,
            is_fixed_rate BOOLEAN NOT NULL,
            valid_until VARCHAR(128) NOT NULL,
            created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
            updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
            PRIMARY KEY(uuid))'
        );
        $this->addSql('COMMENT ON COLUMN payment.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN payment.updated_at IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE payment_uuid_seq CASCADE');
        $this->addSql('DROP TABLE payment');
    }
}
