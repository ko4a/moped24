<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230906061413 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'makes user table timestampable';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user_profile ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE user_profile ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');

        $this->addSql('COMMENT ON COLUMN user_profile.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN user_profile.updated_at IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user_profile DROP created_at');
        $this->addSql('ALTER TABLE user_profile DROP updated_at');
    }
}
