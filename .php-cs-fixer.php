<?php

$finder = PhpCsFixer\Finder::create()
    ->in('./src')
;

$config = new PhpCsFixer\Config();

return $config
    ->setRiskyAllowed(true)
    ->setRules(
        [
            '@PSR1' => true,
            '@PSR2' => true,
            '@PSR12' => true,
            '@Symfony' => true,
            '@DoctrineAnnotation' => true,
            'array_indentation' => true,
            'blank_line_before_statement'       => [
                'statements' => [
                    'break',
                    'continue',
                    'declare',
                    'exit',
                    'goto',
                    'return',
                    'throw',
                    'try',
                    'yield',
                ],
            ],
            'binary_operator_spaces'            => [
                'operators' => [
                    '=>' => null,
                ],
            ],
            'cast_spaces'                       => [
                'space' => 'none',
            ],
            'combine_consecutive_issets'        => true,
            'combine_consecutive_unsets'        => true,
            'concat_space'                      => [
                'spacing' => 'one',
            ],
            'explicit_indirect_variable'        => true,
            'explicit_string_variable'          => true,
            'list_syntax'                       => [
                'syntax' => 'short',
            ],
            'method_chaining_indentation'       => true,
            'multiline_comment_opening_closing' => true,
            'no_mixed_echo_print'               => [
                'use' => 'echo',
            ],
            'no_null_property_initialization'   => true,
            'no_superfluous_phpdoc_tags'        => [
                'allow_mixed' => true,
            ],
            'ordered_imports'                   => [
                'imports_order' => [
                    'const',
                    'class',
                    'function',
                ],
            ],
            'phpdoc_line_span'                  => true,
            'phpdoc_no_empty_return'            => true,
            'phpdoc_order'                      => true,
            'phpdoc_to_comment'                 => [
                'ignored_tags' => ['psalm-suppress'],
            ],
            'phpdoc_return_self_reference'      => [
                'replacements' => [
                    'this'    => 'self',
                    '@this'   => 'self',
                    '$self'   => 'self',
                    '@self'   => 'self',
                    '$static' => 'static',
                    '@static' => 'static',
                ],
            ],
            'return_type_declaration'           => [
                'space_before' => 'none',
            ],
            'self_static_accessor'              => true,
            'simple_to_complex_string_variable' => true,
            'single_line_throw'                 => false,
            'ternary_to_null_coalescing'        => true,
            'declare_strict_types'              => true,
        ]
    )
    ->setFinder($finder)
    ->setCacheFile(__DIR__ . '/.php-cs-fixer.cache');